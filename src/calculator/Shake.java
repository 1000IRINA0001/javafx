package calculator;



import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;
class Shake {
    private TranslateTransition transition;

    Shake(Node node) {
        transition = new TranslateTransition(Duration.millis(70), node);
        transition.setFromX(0);
        transition.setByX(10);
        transition.setCycleCount(3);
        transition.setAutoReverse(true);
    }

    void playAnimation() {
        transition.playFromStart();
    }
}



