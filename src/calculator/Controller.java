package calculator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

@SuppressWarnings("unused")
public class Controller {
    private static final String INPUT_ERROR_MESSAGE = "Введены некорректные данные";
    private static final String DIVISION_ERROR = "На ноль делить нельзя";
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonDiv;

    @FXML
    private Button buttonMult;

    @FXML
    private Button buttonSub;

    @FXML
    private Button buttonClear;

    @FXML
    private TextField result;

    @FXML
    void clear(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        result.clear();
    }

    @FXML
    void add(ActionEvent event) {
        try {
            errors();
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            double sum = first + second;
            result.setText(String.valueOf(sum));
        } catch (Exception e) {
            result.setText(INPUT_ERROR_MESSAGE);
        }
    }
    @FXML
    void div(ActionEvent event) {
        try {
            errors();
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            if (second != 0) {
                double quotient = second / first;
                result.setText(String.valueOf(quotient));
            } else {
                result.setText(DIVISION_ERROR);
            }
        } catch (Exception e) {
            result.setText(INPUT_ERROR_MESSAGE);
        }
    }

    @FXML
    void mult(ActionEvent event) {
        try {
            errors();
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            double composition = first * second;
            result.setText(String.valueOf(composition));
        } catch (Exception e) {
            result.setText(INPUT_ERROR_MESSAGE);
        }
    }

    @FXML
    void sub(ActionEvent event) {
        try {
            errors();
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            double difference = first - second;
            result.setText(String.valueOf(difference));
        } catch (Exception e) {

            result.setText(INPUT_ERROR_MESSAGE);
        }


    }

    private void errors() {
        if (firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            Shake number1Anim = new Shake(firstOperand);
            Shake number2Anim = new Shake(secondOperand);
            number1Anim.playAnimation();
            number2Anim.playAnimation();
            return;
        }
    }

    private void animation(TextField operand) {
        Shake operandAnimation = new Shake(operand);
        operandAnimation.playAnimation();
    }

}