package sample;
import java.net.URL;
import java.util.ResourceBundle;

import com.sun.javafx.scene.control.skin.FXVK;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import sample.animation.Shake;

@SuppressWarnings("unused")
public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private TextField amount;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonClear;

    @FXML
    void add(ActionEvent event) {
        if (firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            Shake number1Anim = new Shake(firstOperand);
            Shake number2Anim = new Shake(secondOperand);
            number1Anim.playAnimation();
            number2Anim.playAnimation();
            return;
        }
        int first = Integer.parseInt(firstOperand.getText());
        int second = Integer.parseInt(secondOperand.getText());
        int sum = first + second;
        amount.setText(String.valueOf(sum));
    }



    @FXML
    void clear(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        amount.clear();
    }

}